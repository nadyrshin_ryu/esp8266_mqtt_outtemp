WIFI_SSID = "ssid"
WIFI_PASS = "password"
MQTT_BrokerIP = "192.168.1.35"
MQTT_BrokerPort = 1883
MQTT_ClientID = "esp-003"
MQTT_Client_user = "user"
MQTT_Client_password = "password"
MQTT_RelayTopicPath = "/ESP/Relays/"
MQTT_Relay_ID = "002"
RELAY_PIN = 6
LED_PIN = 7
BUTTON_PIN = 3
DS18B20_PIN = 5

gpio.mode(RELAY_PIN, gpio.OUTPUT) 
gpio.mode(LED_PIN, gpio.OUTPUT)
gpio.mode(BUTTON_PIN, gpio.INPUT)
gpio.write(RELAY_PIN, gpio.LOW)
gpio.write(LED_PIN, gpio.HIGH)
wifi.setmode(wifi.STATION)
wifi.sta.config(WIFI_SSID, WIFI_PASS)
wifi.sta.connect()

local wifi_status_old = 0
local RelayState_old = 0
local ButtonState_old = 0

local function switchRelay(id, state)
    if (id == MQTT_Relay_ID) then
        if (state == 0) or (state == "0") or (string.lower(state) == "off") then
            if (RelayState_old == 1) then
                RelayState_old = 0
                gpio.write(RELAY_PIN, gpio.LOW)
                gpio.write(LED_PIN, gpio.HIGH)
                --print("RELAY OFF")
                if (m ~= nil) then
                    m:publish(MQTT_RelayTopicPath..MQTT_Relay_ID, "0", 1, 1, function(conn) 
                        --print("Relay state 0 sent") 
                    end)
                end
            end
        else
            if (RelayState_old == 0) then
                RelayState_old = 1
                gpio.write(RELAY_PIN, gpio.HIGH)
                gpio.write(LED_PIN, gpio.LOW)
                --print("RELAY ON")
                if (m ~= nil) then
                    m:publish(MQTT_RelayTopicPath..MQTT_Relay_ID, "1", 1, 1, function(conn) 
                        --print("Relay state 1 sent") 
                    end)
                end
            end
        end
    end
end

-- Сканирование состояния кнопки из таймера 2
tmr.alarm(2, 100, tmr.ALARM_AUTO, function()
    local ButtonState = gpio.read(BUTTON_PIN)

    -- Нажатие кнопки (задний фронт на gpio0)
    if (ButtonState == 0) and (ButtonState_old == 1) then
        --print("Button pressed")
        if (RelayState_old == 0) then
            switchRelay(MQTT_Relay_ID, 1)
        else
            switchRelay(MQTT_Relay_ID, 0)
        end
    end
    ButtonState_old = ButtonState
end)

tmr.alarm(0, 5000, 1, function()
    print("tmr0 "..wifi_status_old.." "..wifi.sta.status())

    if wifi.sta.status() == 5 then -- подключение есть
        if wifi_status_old ~= 5 then -- Произошло подключение к Wifi, IP получен
            print(wifi.sta.getip())

            m = mqtt.Client(MQTT_ClientID, 120, MQTT_Client_user, MQTT_Client_password)

            -- Определяем обработчики событий от клиента MQTT
            m:on("connect", function(client) print ("connected") end)
            m:on("offline", function(client) 
                tmr.stop(1)
                print ("offline") 
            end)
            m:on("message", function(client, topic, data) 
                --print(topic .. ":" ) 
                if data ~= nil then
                    --print(data)
                end

                local _, RelayPos = string.find(topic, MQTT_RelayTopicPath.."(%w)")
                local Relay = string.sub(topic, RelayPos)
                --print(Relay)
                if data ~= nil then
                    switchRelay(Relay, data)
                end
            end)

            m:connect(MQTT_BrokerIP, MQTT_BrokerPort, 0, 1, function(conn) 
                print("connected")
            
                -- Подписываемся на топики если нужно
                m:subscribe(MQTT_RelayTopicPath.."#",0, function(conn) 
					print("Subscribed!")
				end)
                    
                tmr.alarm(1, 10000, 1, function()

                    -- Поиск датчиков DS18B20
                    ow.setup(DS18B20_PIN)
                    local ds18b20_adr = {}
                    ow.reset_search(DS18B20_PIN)
                    repeat
                        local addr = ow.search(DS18B20_PIN)
                        if (addr ~= nil) then
                            print("ADDR:")
                            print(addr:byte(1,8))
                            crc = ow.crc8(string.sub(addr,1,7))
                            if crc == addr:byte(8) then
                                --print("Address CRC OK")
                                if (addr:byte(1) == 0x10) or (addr:byte(1) == 0x28) then
                                    print("DS18S20 family")
                                    table.insert(ds18b20_adr, addr)
                                end
                            end
                        end
                    until (addr == nil)
                    
                    print("Sensors "..#ds18b20_adr)

                    -- Старт измерения всех датчиков
                    for i = 1, #ds18b20_adr do
                        local addr = ds18b20_adr[i];

                        ow.reset(DS18B20_PIN)
                        ow.select(DS18B20_PIN, addr)
                        ow.write(DS18B20_PIN, 0x44, 1)
                    end
                    
                    -- Задержка на время измерения
                    tmr.delay(1000000)

                    -- Чтение результатов измерения из всех датчиков
                    for i = 1, #ds18b20_adr do
                        local addr = ds18b20_adr[i];

                        present = ow.reset(DS18B20_PIN)
                        ow.select(DS18B20_PIN, addr)
                        ow.write(DS18B20_PIN, 0xBE, 1)
                        --print("P="..present)  
                        data = nil
                        data = string.char(ow.read(DS18B20_PIN))
                        for i = 1, 8 do
                            data = data .. string.char(ow.read(DS18B20_PIN))
                        end
                        --print("DATA:")
                        --print(data:byte(1,9))
                        crc = ow.crc8(string.sub(data,1,8))
                        --print("CRC="..crc)
                        if crc == data:byte(9) then
                            t = (data:byte(1) + data:byte(2) * 256)
                            if (t > 0x7FFF) then t = t - 0x10000 end
                            t1 = (t * 625) / 10000
                            print("Temp:"..t1.." C")
                            m:publish("/ESP/ESP003/TEMP"..i, t1, 0, 0, function(conn) print("sent") end)
                        end                   
                        tmr.wdclr()
                    end
                end)
            end)
        else
            -- подключение есть и не разрывалось, ничего не делаем
        end
    else
        print("Reconnect "..wifi_status_old.." "..wifi.sta.status())
        tmr.stop(1)
        wifi.sta.connect()
    end

    -- Запоминаем состояние подключения к Wifi для следующего такта таймера
    wifi_status_old = wifi.sta.status()
end)
